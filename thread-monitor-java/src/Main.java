
public class Main {
	private static Thread t0;
	private static Thread t1;
	private static Thread t2;

	public static void main(String[] args) {
		//
		t0 = new Thread(){
			@Override
			public void run() {
				try {
					synchronized (t1) {
						System.out.println("t0: Iniciar o tempo de espera por t1");
						t1.wait();
						System.out.println("t0: Finalizar o tempo de espera por t1");
					}
					synchronized (this) {
						System.out.println("t0: Liberar quem estiver esperando por t0");
						notify();
					}
					//
					System.out.println(".0");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		t1 = new Thread(){
			public void run() {
				try {
					synchronized (t0) {
						System.out.println("t1: Iniciar o tempo de espera por t0");
						t0.wait();
						System.out.println("t1: Finalizar o tempo de espera por t0");
					}
					synchronized (this) {
						System.out.println("t1: Liberar quem estiver esperando por t1");
						notify();
					}
					//
					System.out.println(".1");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			};
		};
		t2 = new Thread(){
			public void run() {
				synchronized (t0) {
					System.out.println("t2: Liberar quem estiver esperando por t0");
					t0.notify();
				}
			};
		};
		//
		
		t1.start();
		t2.start();
		t0.start();
		
		
	}
	
}
