//package default

import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;


public class Server {
    public static void log(String info){
        System.out.println("[log] "+ info);
    }

    public static void main(String[] args) throws IOException, InterruptedException{
        //log
        log("Iniciando o servidor");
        //create server = 0.0.0.0:109999
        ServerSocket ss = new ServerSocket();
        
        //log
        log("Aguardando uma conexão remota");
        //listen and accept
        // --> while(true){ begin looping
        Socket s = ss.accept();
        //log
        log("Conexão remota recebida");
        //log
        log("Fazendo leitura do stream");
        //read stream
        byte[] bs = new byte[1024];//<---
        s.getInputStream().read(bs);
        String inStr = new String(bs).trim();
        //log
        log("Mensagem recebida: " + inStr);
        //log
        log("Processando mensagem"); Thread.sleep(20000);
        //processing
        String output = (inStr + ", Client");
        //log
        log("Respondendo o cliente (escrevendo no stream)");
        //write stream
        s.getOutputStream().write(output.getBytes());
        //log
        log("Encerrando a conexão");
        //close socket
        s.close();
        // --> } end looping
        //log
        log("Encerrar o servidor");
        //close server
        ss.close();
    }
}