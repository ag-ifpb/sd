//package default

import java.io.IOException;
import java.net.UnknownHostException;
import java.net.Socket;


public class Client {

    public static void log(String info){
        System.out.println("[log] "+ info);
    }

    public static void main(String[] args) throws IOException, UnknownHostException{
        //log
        log("Iniciando o cliente e abrindo uma conexão");
        //create a socket
        Socket s = new Socket("localhost", 10999);
        //log
        log("Envia uma requisição (escrevendo no stream)");
        //write stream
        s.getOutputStream().write("Hello".getBytes());
        //s.getOutputStream().write("GET / HTTP/1.0\n\r\n\r".getBytes());
        //log
        log("Aguardando o servidor responder");
        //read stream
        byte[] bs = new byte[1024];
        s.getInputStream().read(bs);
        String inStr = new String(bs).trim();
        //log
        log("Leitura realizada (resposta recebida)");
        //close socket
        s.close();
        //log
        log("Encerrando a conexão e imprimindo o resultado");
        //print result
        System.out.println(inStr);
    }
}