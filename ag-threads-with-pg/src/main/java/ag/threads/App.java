//--select * from tb where edited = false and deleted = true
//select cast(max(name) as bigint) - cast(min(name) as bigint) from tb 
//--1 thread(s): "16362" (10000)"154929"
//--2 thread(s): "" (10000)"103478"
//--3 thread(s): "12363" (10000)"114808"
//select count(*) from tb where name = ('Ari ' || id)
package ag.threads;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.hsqldb.lib.StopWatch;

/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) throws SQLException, InterruptedException {
		//pg uri connection
		String pguri = "jdbc:postgresql://localhost:5432/docker?user=docker&password=docker";
		Connection conn = DriverManager.getConnection(pguri);
		conn.setAutoCommit(true);
		//clear database
		Operation.clear(conn);
		//operation instance
		Operation op = new Operation(conn);
		BlockingChannel channel = new BlockingChannel();
		BlockingQueue monitor0 = new BlockingQueue(1);//0 to 1
		BlockingQueue monitor1 = new BlockingQueue(1);//1 to 2
		//
		Volatile vol = new Volatile();
		//threads
		ExecutorService executor1 = Executors.newFixedThreadPool(1);
		ExecutorService executor2 = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 10000; i++) {
			//bloqueia se já tiver em uso
			System.out.println("0:bloqueado");
			//channel.lock();//<-- por que?
			int id = i;
			System.out.println("1:desbloqueado");
			//
			Runnable t0 = new InsertOperation(id, op, vol, monitor0);
			Runnable t1 = new UpdateOperation(id, op, vol, monitor0, monitor1);
			Runnable t2 = new DeleteOperation(id, op, vol, monitor1, channel);
			executor1.execute(t0);
			executor1.execute(t1);
			executor2.execute(t2);
			//
//			if (Math.random() > 0.7) {
//				vol.markAsSuspend();
//				System.out.println("parando...");
//				Thread.sleep(3000);
//				vol.markAsUnsuspend(vol);
//				System.out.println("voltando...");
//			}
		}
		executor1.shutdown();
		executor2.shutdown();
	}
}
