package ag.ifpb;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

import org.junit.Test;

public class IDManagerTest {
	
	private static void myTest(){
		for (int i = 0; i < 1000000; i++) {
			try {
				Socket s = new Socket("localhost", 10990);
				int count = 0;
				byte[] b = new byte[4];
				while (count < 4){
					count += s.getInputStream().read(b);
					if (count < 4) System.out.println("count: " + count);
				}
				ByteBuffer buff = ByteBuffer.wrap(b);
				int v = buff.getInt();
				if (v % 1000 == 0) System.out.println("ID: " + v);
				s.close();
				Thread.sleep(1);
			} catch (InterruptedException | IOException e){
				e.printStackTrace();
			} 
		}
	}

	@Test
	public void test() {
		myTest();
	}
	
	public static void main(String[] args) {
		myTest();
	}

}
