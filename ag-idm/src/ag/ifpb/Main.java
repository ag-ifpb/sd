package ag.ifpb;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		//
		IDManagerImpl idm = new IDManagerImpl();
		idm.fill(1000000);
		//
		IDManagerStub stub = new IDManagerStub(idm);
		stub.start();
	}
}
