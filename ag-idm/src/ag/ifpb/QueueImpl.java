package ag.ifpb;

import java.util.Queue;
import java.util.concurrent.LinkedTransferQueue;

public class QueueImpl {
	Queue<Integer> queue = new LinkedTransferQueue<Integer>();
	
	public Integer dequeue(){
		return queue.poll();
	}
	
	public void enqueue(Integer value){
		queue.add(value);
	}
	

}
