package ag.ifpb;

public class IDManagerImpl implements IDManager{
	QueueImpl queue = new QueueImpl();

	@Override
	public Integer id() {
		return queue.dequeue();
	}
	
	public void fill(int size){
		for (int i = 1; i <= size; i++){
			queue.enqueue(i);
			System.out.print(".");
		}
		System.out.println();
		System.out.println("Inserido todos");
	}
	
}
