package ag.ifpb;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class IDManagerStub {
	private ServerSocket ss;
	private IDManager idm;
	
	public IDManagerStub(IDManager m) throws IOException {
		ss = new ServerSocket(10990, 100);
		idm = m;
	}

	private void send(Socket s) throws IOException{
		ByteBuffer buff = ByteBuffer.allocate(Integer.BYTES);
		buff.putInt(idm.id());//write 4 bytes
		s.getOutputStream().write(buff.array());
		s.getOutputStream().flush();
		buff.clear();
	}
	
	public void start() throws IOException{
		Executor executor = Executors.newFixedThreadPool(4);
		while (true) {
			//
			Socket s = ss.accept();
			//
			Runnable r = new Runnable() {
				@Override
				public void run() {
					try {
						send(s);
						s.close();
					}catch(IOException e){
						e.printStackTrace();
					}
				}
			};
			//
			executor.execute(r);
		}
	}
	
}
